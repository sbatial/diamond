module Main where

import Data.Char (ord, toUpper)

main :: IO ()
main = getChar >>= \x -> mapM_ putStrLn (upperHalf x ++ lowerHalf x)
  where
    half transform letter = map (letterRow letter) $ transform (letterList letter)
    upperHalf = half id
    lowerHalf = half (tail . reverse)

letterList :: Char -> [Char]
letterList n = ['A' .. toUpper n]

letterRow :: Char -> Char -> String
letterRow goal curr = leftPad ++ isNotA curr ++ centerPad ++ [curr]
  where
    dist = goal -. curr
    leftPad = replicate dist ' '
    centerPad = replicate (2 * (curr -. 'A') - 1) ' '

    chr1 -. chr2 = ord chr1 - ord chr2

    isNotA 'A' = ""
    isNotA c = [c]
