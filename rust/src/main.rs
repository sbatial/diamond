#![allow(unused)]
fn main() {
    let target = 'J';
    diamond_imperative(target);
    diamond_iterative_simple(target);
    diamond_iterative_fancy(target);
}

fn diamond_imperative(target: char) -> () {
    let target = if target < 'A' {
        (target as u8 + ('a' as u8 - 'A' as u8)) as char
    } else {
        target
    };
    for c in 'A'..=target {
        println!(
            "{}{}{}{}",
            " ".repeat((target as usize).saturating_sub(c as usize)),
            c,
            " ".repeat((2 * (c as usize).saturating_sub('A' as usize)).saturating_sub(1)),
            if c != 'A' { c } else { ' ' }
        )
    }
    for c in 'B'..=target {
        let chr = ('A' as usize + (target as usize).saturating_sub(c as usize)) as u8 as char;
        println!(
            "{}{}{}{}",
            " ".repeat(((c as usize).saturating_sub('A' as usize))),
            chr,
            " ".repeat((2 * (target as usize).saturating_sub(c as usize)).saturating_sub(1)),
            if chr != 'A' { chr } else { ' ' }
        )
    }
}

fn diamond_iterative_simple(target: char) {
    ('A'..=target).chain(('A'..target).rev()).for_each(|chr| {
        let start = target as usize - chr as usize;

        println!(
            "{}{}{}{}",
            " ".repeat(start),
            chr,
            " ".repeat(2 * (target as usize - 65).saturating_sub(start + 1) + 1),
            if chr != 'A' { chr } else { ' ' }
        )
    });
}

fn diamond_iterative_fancy(target: char) {
    ('A'..=target).chain(('A'..target).rev()).for_each(|chr| {
        let start = target as usize - chr as usize + 1;

        println!(
            "{chr: >start$} {: >inner$}",
            if chr != 'A' { chr } else { ' ' },
            inner = 2 * (target as usize - 65).saturating_sub(start) + 1
        )
    });
}

fn golfed() {
    std::env::args_os()
        .map(|c| c.to_str().unwrap().chars().next().unwrap() as usize)
        .map(|c| {
            (65..=(2 * c - 65))
                .map(|x| c - c.abs_diff(x))
                .map(|s| (c - s + 1, 2 * (s - 65), s as u8 as char))
                .for_each(|(l, b, c)| println!("{c:>l$}{:>b$}", if 1 < b { c } else { ' ' }))
        })
        .min();
}
