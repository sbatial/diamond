{
  projectRootFile = "flake.nix";
  programs = {
    alejandra.enable = true;
    rustfmt.enable = true;
    ormolu.enable = true;
  };
}
