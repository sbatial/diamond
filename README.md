[![built with nix](https://builtwithnix.org/badge.svg)](https://builtwithnix.org)

# Diamond

Implementations of the 'letter diamond'.

## Languages

### Haskell

### Rust

![regular diamond made up of letters, going from A to L and back](output_l.png)

It started by having a go at it because I thought it was interesting and someone asked me how I would implement it.
It lead to me aggressively optimizing line-count because I wanted to see how low I can go.

I'm currently at 18 lines while adhering to the rust formatter and while trying to keep the code as readable as possible.
