{
  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
    naersk.url = "github:nix-community/naersk";
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    treefmt-nix.url = "github:numtide/treefmt-nix";
    rust-overlay.url = "github:oxalica/rust-overlay";
  };

  outputs = {
    self,
    flake-utils,
    naersk,
    nixpkgs,
    treefmt-nix,
    rust-overlay,
  }:
    flake-utils.lib.eachDefaultSystem (
      system: let
        # TODO: Factor out haskell/rust into their respective subfolders
        hs-overlay = final: prev: {
          haskell =
            prev.haskell
            // {
              packageOverrides = hfinal: hprev:
                prev.haskell.packageOverrides hfinal hprev
                // {
                  diamond = hfinal.callCabal2nix "diamond" ./haskell {};
                };
            };
          diamond = final.haskell.lib.compose.justStaticExecutables final.haskellPackages.diamond;
        };

        overlays = [(import rust-overlay) hs-overlay];
        pkgs = (import nixpkgs) {
          inherit system overlays;
        };

        hspkgs = pkgs.haskellPackages;

        toolchain = pkgs.rust-bin.fromRustupToolchainFile ./rust/rust-toolchain.toml;

        naersk' = pkgs.callPackage naersk {
          cargo = toolchain;
          rustc = toolchain;
        };
        treefmtEval = treefmt-nix.lib.evalModule pkgs ./treefmt.nix;
      in {
        # For `nix fmt`
        formatter = treefmtEval.config.build.wrapper;

        # For `nix build` & `nix run`:
        packages.rust = naersk'.buildPackage {src = ./rust;};
        packages.haskell = pkgs.diamond;

        # For `nix develop`:
        devShell = hspkgs.shellFor {
          withHoogle = true;
          packages = p: [p.diamond];
          buildInputs = [
            hspkgs.cabal-install
            hspkgs.haskell-language-server
            hspkgs.hlint
            hspkgs.ormolu
          ];
          nativeBuildInputs = [toolchain];
        };

        # for `nix flake check`
        checks = {
          formatting = treefmtEval.config.build.check self;
          test = naersk'.buildPackage {
            src = ./.;
            mode = "test";
          };
        };
      }
    );
}
